from app.rate.exceptions import RateNotExistException
from app.rate.models import Rate
from app.rate.schemas import SCost, SRatesSchedule, SInsuranceCost


class RateService:
    def __init__(self):
        self._rate_model = Rate

    async def add_bulk_rates(self, schedule: SRatesSchedule) -> None:
        bulk_rates = []
        for date, rates in schedule.__root__.items():
            for rate in rates.__root__:
                bulk_rates.append(
                    self._rate_model(date=date, cargo_type=rate.cargo_type, rate=rate.rate)
                )

        await self._rate_model.bulk_create(bulk_rates)

    async def get_declared_cost(self, cost: SCost) -> SInsuranceCost:
        rate = await self._rate_model.get_or_none(date=cost.date, cargo_type=cost.cargo_type)
        if not rate:
            raise RateNotExistException

        insurance_cost = float(rate.rate) * cost.declared_cost

        cost = SInsuranceCost(
            declared_cost=cost.declared_cost,
            cargo_type=cost.cargo_type,
            rate=float(rate.rate),
            insurance_cost=insurance_cost,
            total_cost=cost.declared_cost + insurance_cost
        )

        return cost
