from fastapi import APIRouter, Depends, status

from app.auth.dependencies import get_current_user
from app.auth.models import User
from app.rate.dependencies import get_rate_service
from app.rate.schemas import SRatesSchedule, SMessage, SCost, SInsuranceCost
from app.rate.services import RateService

rate_router = APIRouter(
    prefix="/rate",
    tags=["Rate"]
)


@rate_router.post("/load", status_code=status.HTTP_201_CREATED)
async def load_rates(
        rates_schedule: SRatesSchedule,
        user: User = Depends(get_current_user),
        rate_service: RateService = Depends(get_rate_service)
) -> SMessage:
    """Load actual rates"""
    await rate_service.add_bulk_rates(rates_schedule)
    return SMessage(message="Rates have been successfully loaded")


@rate_router.get("")
async def calculate_cost(
    cost_params: SCost = Depends(),
    rate_service: RateService = Depends(get_rate_service)
) -> SInsuranceCost:
    """Calculate a coast of insurance
    instead of date, cargo type and declarative cost.
    Date must be in format YYYY-MM-DD"""

    calculated_cost = await rate_service.get_declared_cost(cost_params)
    return calculated_cost
