from datetime import date
from typing import List, Dict

from pydantic import BaseModel, Field, validator


class SRate(BaseModel):
    cargo_type: str
    rate: float

    @validator("cargo_type", pre=True)
    def to_lowercase(cls, v: str):
        return v.lower()


class SRatesList(BaseModel):
    __root__: List[SRate]


class SRatesSchedule(BaseModel):
    __root__: Dict[date, SRatesList] = Field(
        example="""
        {
          "2023-06-01": [
            {
              "cargo_type": "Glass",
              "rate": 0.04
            },
            {
              "cargo_type": "Other",
              "rate": 0.01
            }
          ],
          "2023-07-01": [
            {
              "cargo_type": "Glass",
              "rate": 0.035
            },
            {
              "cargo_type": "Other",
              "rate": 0.015
            }
          ]
        }
        """
    )


class SCost(BaseModel):
    declared_cost: float
    date: date
    cargo_type: str

    @validator("cargo_type", pre=True)
    def to_lowercase(cls, v: str):
        return v.lower()


class SInsuranceCost(BaseModel):
    declared_cost: float
    cargo_type: str
    rate: float
    insurance_cost: float
    total_cost: float


class SMessage(BaseModel):
    message: str
