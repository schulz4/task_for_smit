from tortoise import fields
from tortoise.models import Model


class Rate(Model):
    id = fields.IntField(pk=True)
    date = fields.DateField(null=False)
    cargo_type = fields.CharField(max_length=100)
    rate = fields.DecimalField(max_digits=6, decimal_places=4)

    class Meta:
        unique_together = [
            ("date", "cargo_type")
        ]

    def __str__(self):
        return f"{self.date}: {self.cargo_type}-{self.rate}"
