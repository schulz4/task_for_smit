from fastapi import status

from app.exceptions import AppException


class RateNotExistException(AppException):
    status_code = status.HTTP_404_NOT_FOUND
    detail = "Rate doesn't exist with current date or cargo_type"
