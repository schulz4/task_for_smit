from datetime import datetime, timedelta
from typing import Optional, Literal

from jose import jwt
from passlib.context import CryptContext
from pydantic import EmailStr
from tortoise.exceptions import BaseORMException

from app.auth.exceptions import TokenNotAddedException, UserHasNotSession, IncorrectEmailOrPasswordException, \
    TokenExpiredException, UserAlreadyExistsException
from app.auth.models import User, RefreshSession
from app.auth.schemas import TokenInfo, STokens
from app.auth.utils import check_jwt
from app.config import get_settings


class AuthorizationService:
    def __init__(self) -> None:
        self._user_model = User
        self._session_model = RefreshSession
        self.settings = get_settings()
        self._pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def _create_token(self, token_name: Literal["refresh", "access"], exp_min: int, data: Optional[dict]) -> TokenInfo:
        to_encode = data.copy() if data else {}
        expire = datetime.utcnow() + timedelta(minutes=exp_min)
        to_encode.update(exp=expire)
        encode_jwt = jwt.encode(
            to_encode, self.settings.auth.KEY, self.settings.auth.ALGORITHM
        )
        return TokenInfo(token_name=token_name, token=encode_jwt, expires_in=expire)

    def create_tokens(self, data: dict) -> STokens:
        access_token = self._create_token(
            token_name="access",
            data=data,
            exp_min=self.settings.tokens.ACCESS_TOKEN_EXPIRES
        )
        refresh_token = self._create_token(
            token_name="refresh",
            data=data,
            exp_min=self.settings.tokens.REFRESH_TOKEN_EXPIRES
        )
        return STokens(access_token=access_token, refresh_token=refresh_token)

    async def authenticate_user(self, email: EmailStr, password: str) -> User:
        user: User = await self._user_model.get_or_none(email=email)
        if not user or not self._verify_password(password, user.hashed_password):
            raise IncorrectEmailOrPasswordException
        return user

    async def get_user(self, user_id) -> User:
        user = await self._user_model.get_or_none(id=user_id)
        return user

    async def check_user(self, email: EmailStr) -> User:
        user = await self._user_model.get_or_none(email=email)
        return user

    async def add_user(self, email: EmailStr, password: str) -> None:
        existing_user = await self.check_user(email=email)
        if existing_user:
            raise UserAlreadyExistsException

        hashed_password = self._get_password_hash(password)

        await self._user_model.create(
            email=email,
            hashed_password=hashed_password
        )

    async def create_refresh_session(self, **values) -> None:
        try:
            await self._session_model.create(**values)
        except BaseORMException:
            raise TokenNotAddedException

    async def check_refresh_token(self, token: str) -> str:
        try:
            user_id = check_jwt(token=token)
            session = await self._session_model.get_or_none(refresh_token=token, user_id=user_id)
        except TokenExpiredException as e:
            await self._session_model.filter(id=session.id).delete()
            raise e

        if not session:
            raise UserHasNotSession

        await self._session_model.filter(id=session.id).delete()

        return user_id

    async def delete_refresh_session(self, user_id) -> None:
        await self._session_model.filter(user_id=user_id).delete()

    def _get_password_hash(self, password: str) -> str:
        return self._pwd_context.hash(password)

    def _verify_password(self, plain_password: str, hashed_password: str) -> bool:
        return self._pwd_context.verify(plain_password, hashed_password)
