from datetime import datetime
from uuid import uuid4

from tortoise import fields
from tortoise.models import Model


class User(Model):
    id = fields.IntField(pk=True)
    email = fields.CharField(max_length=255, null=False)
    hashed_password = fields.CharField(max_length=255, null=False)
    full_name = fields.CharField(max_length=100, null=True)


class RefreshSession(Model):
    id = fields.UUIDField(pk=True, default=uuid4)
    user = fields.ForeignKeyField("models.User", related_name="sessions", on_delete="CASCADE")
    refresh_token = fields.CharField(max_length=1024, null=False)
    expires_in = fields.DateField(null=False)
    created_at = fields.DateField(default=datetime.utcnow)
