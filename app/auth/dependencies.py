from typing import Annotated

from fastapi.params import Depends
from fastapi.security import OAuth2PasswordBearer

from app.auth.exceptions import NotUserID
from app.auth.services import AuthorizationService
from app.auth.utils import check_jwt


def get_auth_service(

) -> AuthorizationService:
    return AuthorizationService()


def get_oauth2_scheme():
    return OAuth2PasswordBearer(tokenUrl="auth/login")


async def get_current_user(
        token: Annotated[str, Depends(get_oauth2_scheme())],
        auth_service: AuthorizationService = Depends(get_auth_service)
):

    user_id = check_jwt(token)

    user = await auth_service.get_user(user_id=user_id)
    if not user:
        raise NotUserID

    return user
