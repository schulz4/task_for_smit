from datetime import date
from typing import Literal

from pydantic import BaseModel, EmailStr
from tortoise.contrib.pydantic import pydantic_model_creator

from app.auth.models import User


class SUserAuth(BaseModel):
    email: EmailStr
    password: str


SUser = pydantic_model_creator(User, name="User")


class TokenInfo(BaseModel):
    token_name: Literal["refresh", "access"]
    token: str
    expires_in: date


class SAccessToken(BaseModel):
    access_token: str
    token_type: str


class STokens(BaseModel):
    refresh_token: TokenInfo
    access_token: TokenInfo
