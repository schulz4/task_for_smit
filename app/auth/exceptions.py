from fastapi import status

from app.exceptions import AppException


class AuthException(AppException):
    status_code = status.HTTP_403_FORBIDDEN
    detail = "You can not be authorized"
    headers = None


class EmailInvalidException(AuthException):
    status_code = status.HTTP_409_CONFLICT
    detail = "Email is invalid or not exist"


class UserAlreadyExistsException(AuthException):
    status_code = status.HTTP_409_CONFLICT
    detail = "User already exists"


class IncorrectEmailOrPasswordException(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Incorrect email or password"


class TokenExpiredException(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Token expired"


class TokenAbsentException(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Token absents"


class TokenNotCorrectException(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Not correct token format"
    headers = {"WWW-Authenticate": "Bearer"}


class NotUserID(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED


class TokenNotAddedException(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Can't create session"


class UserHasNotSession(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "User has not session"


class InvalidRefreshToken(AuthException):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Invalid refresh token"
