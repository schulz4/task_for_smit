from jose import jwt, ExpiredSignatureError, JWTError
from passlib.context import CryptContext

from app.auth.exceptions import TokenNotCorrectException, TokenExpiredException
from app.config import get_settings

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def check_jwt(token: str) -> str:
    settings = get_settings()
    try:
        payload = jwt.decode(token, settings.auth.KEY, settings.auth.ALGORITHM)
        user_id = payload.get("sub")
        if not user_id:
            raise TokenNotCorrectException

        return user_id

    except (ExpiredSignatureError, JWTError) as e:
        if isinstance(e, ExpiredSignatureError):
            raise TokenExpiredException
        else:
            raise TokenNotCorrectException
