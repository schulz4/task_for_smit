from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

from app.auth.router import auth_router, user_router
from app.config import get_settings
from app.rate.router import rate_router

app = FastAPI()

app.include_router(auth_router)
app.include_router(user_router)
app.include_router(rate_router)

register_tortoise(
    app,
    db_url=get_settings().db_setting.database_url,
    modules={"models": ["app.auth.models", "app.rate.models"]},
    generate_schemas=True,
    add_exception_handlers=True,
)

