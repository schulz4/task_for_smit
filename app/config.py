from dataclasses import dataclass
from functools import lru_cache
from typing import Literal

from pydantic import BaseSettings


class BaseConfig(BaseSettings):
    MODE: Literal["DEV", "TEST", "PROD"]
    LOG_LEVEL: Literal["DEBUG", "INFO", "WARN", "ERROR", "FATAL"]

    class Config:
        env_file = ".env"


class DBSetting(BaseConfig):
    DB_HOST: str
    DB_NAME: str
    DB_PORT: int
    DB_PASS: str
    DB_USER: str

    @property
    def _prefix(self):
        return "test_" if self.MODE == "TEST" else ""

    @property
    def database_url(self):

        user = f"{self.DB_USER}:{self.DB_PASS}"
        database = f"{self.DB_HOST}:{self.DB_PORT}/{self._prefix}{self.DB_NAME}"
        return f"asyncpg://{user}@{database}"


class AuthSetting(BaseConfig):
    ALGORITHM: str
    KEY: str


class TokenSettings(BaseConfig):
    ACCESS_TOKEN_EXPIRES: int
    REFRESH_TOKEN_EXPIRES: int


@dataclass
class Settings:
    base: BaseConfig
    db_setting: DBSetting
    auth: AuthSetting
    tokens: TokenSettings


@lru_cache
def get_settings():
    return Settings(
        base=BaseConfig(),
        db_setting=DBSetting(),
        auth=AuthSetting(),
        tokens=TokenSettings(),
    )
