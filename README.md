# FastAPI Employer Test Application for SMIT.Studio

This is a simple test application built with FastAPI, designed for employers to evaluate potential candidates. The application utilizes a Docker Compose file for easy deployment and management.

## Prerequisites

Before running the application, ensure you have the following installed:

Docker
Docker Compose

## Getting Started

To get started with the application, follow these steps:

#### 1. Clone the repository to your local machine:

`git clone https://gitlab.com/schulz4/task_for_smit.git`

#### 2. Copy file .env.dist to .env and fill it with the required data

#### 3. Build and start the Docker containers using Docker Compose:

`docker-compose up` 

#### 4. Once the containers are up and running, you can access the application at http://localhost:9000 in your web browser. At http://localhost:9000/docs you can use documentation of application

## Usage

The application provides the following API endpoints:

POST /auth/register: Registration for users.

POST /auth/login: Get tokens for private endpoints.

POST /auth/refresh: Get a new pair when the access token expires.

POST /auth/logout: User logout.

GET /users/me: Retrieve information about current authorized user

POST /rate/load: Loading of current rates.

GET /rate: Retrieve a cost of insurance instead of date and type of cargo.


You can interact with these endpoints using tools such as cURL or a REST API client like Postman.

